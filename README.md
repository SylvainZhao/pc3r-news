Binôme: 

- Wenzhuo ZHAO
- Chengyu YANG

# *News*: une application web d'actualités

<del>**Utilisation en ligne: http://64.225.1.187:5000/**</del>

Documents de Back End API: https://documenter.getpostman.com/view/10263827/TzCQc7Uu

Rapport du projet: voir le fichier *Rapport.md*

<center class="half">
	<img src="https://raw.githubusercontent.com/valeeraZ/-image-host/master/macbook.png" style="zoom: 25%;" />
	<img src="https://raw.githubusercontent.com/valeeraZ/-image-host/master/iPhone.png" style="zoom: 10%;" />
</center>
